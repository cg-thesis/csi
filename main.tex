\documentclass[10pt]{beamer}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{mathpartir}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{graphicx}
\usepackage{color}
\usepackage{minted}
\usepackage{animate}
\usepackage{graphicx}
\usepackage{lisa}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric}

\usetheme{Boadilla}
\usecolortheme{rose}
\useinnertheme{circles}
\institute[]{%
CIFRE Ph.D. Student\\
Nomadic Labs - Université de Paris
}
\author[]{Colin González}

\title[]{Using Structured Spreadsheets to Develop Smart Contracts}

\AtBeginSection[]{
  \ifnumcomp{\value{section}}{=}{1}{}
  {
    \begin{frame}
      \tableofcontents[currentsection]
    \end{frame}
  }
}
\AtBeginSubsection[]{
  \begin{frame}
    \vfill
    \centering
    \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
      \usebeamerfont{title}\insertsubsectionhead\par%
    \end{beamercolorbox}
    \vfill
  \end{frame}
}

\makeatletter
\setbeamertemplate{title page}{
  \vbox{}
  \vfill
  \begingroup
    \centering
    \begin{beamercolorbox}[sep=8pt,center]{title}
      \usebeamerfont{title}\inserttitle\par%
      \ifx\insertsubtitle\@empty%
      \else%
        \vskip0.25em%
        {\usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\par}%
      \fi%
    \end{beamercolorbox}%
    \vskip1em\par
    \begin{beamercolorbox}[sep=8pt,center]{author}
      \usebeamerfont{author}\insertauthor
    \end{beamercolorbox}
    \begin{beamercolorbox}[sep=8pt,center]{institute}
      \usebeamerfont{institute}\insertinstitute
    \end{beamercolorbox}
    % ----------------------- new
    \begin{beamercolorbox}[sep=8pt,center]{institute}
      \usebeamerfont{institute}Under the supervision of \vskip0.75em
      Ralf Treinen (IRIF, Université de Paris) \hfill
      Benjamin Canou (Nomadic Labs) \vskip1em
      Adrien Guatto (IRIF, Université de Paris)\hfill
      Yann Régis-Gianas (Nomadic Labs)
    \end{beamercolorbox}
    % ------------------------
    \begin{beamercolorbox}[sep=8pt,center]{date}
      \usebeamerfont{date}{2021-09-29}
    \end{beamercolorbox}\vskip0.5em
    {\usebeamercolor[fg]{titlegraphic}\inserttitlegraphic\par}
  \endgroup
  \vfill
}
\makeatother

\tikzset{
  buffer/.style={
    draw,
    regular polygon,
    regular polygon sides=3,
    node distance=2cm,
    minimum height=4em,
    label={[align=center]corner 1:Accessibility},
    label={[align=center]corner 2:Reliability},
    label={[align=center]corner 3:Expressivity},
  },
  invisible/.style={opacity=0},
  visible on/.style={alt={#1{}{invisible}}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}}
  },
}

\tikzstyle{language} = [ellipse, minimum height=1cm, text centered, draw=black]

\tikzstyle{arrow} = [thick,->,>=stealth]

\beamertemplatenavigationsymbolsempty

\graphicspath{{images}}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}
\section{Introduction}
\subsection{Smart Contracts or Programmable Transactions}
\begin{frame}{Programmable Transactions}
  \begin{itemize}
  \item How to make a \textbf{secure} transaction \textbf{without}
    a trusted third-party?
  \item You use a blockchain, a \textbf{distributed} and
    \textbf{non forgeable} ledger!
  \item Transactions are chosen and performed using a
    \textbf{consensus protocol}.
  \end{itemize}
  \begin{center}
    \uncover<2->{
      \framebox{
        \large{Bonus: they are \textbf{programmable}.}
      }
    }
  \end{center}
\end{frame}
\begin{frame}{You Said Programmable?}
  \begin{itemize}
  \item \textcolor{red}{Yes!} We call them
      \textbf{smart contracts}.
    \item Smart contracts are publicly hosted and executed by
      the blockchain.
  \item The code of this not modifiable.
  \item Hence the contract trait.
  \end{itemize}
\end{frame}
\subsection{How Can End-Users Develop Smart Contracts?}
\begin{frame}{A Simple Accounting Task}
  \begin{itemize}
  \item Our accountant Bill,\\ wants to develop, deploy and monitor
    a smart contract.
    \begin{exampleblock}{Informal spec}
      This contract \textbf{collects} \textbf{deposits};
      \textbf{only} Alice can withdraw the collected coins.
    \end{exampleblock}
  \item Bill can model it using a spreadsheet.
  \end{itemize}
\end{frame}
\begin{frame}[fragile]{The Spreadsheet}
  \begin{itemize}
  \item Two inputs, \textbf<2->{user} and \textbf<2->{deposit}
  \item A state to compute the \textbf<3->{collected} amount.
  \item One output, the \textbf<4->{operations} to commit.
  \item Bill pulls down the last line to fill the spreadsheet.
  \end{itemize}
    \begin{center}
      \begin{figure}
        \scalebox{0.57}{
          \begin{tabular}{|c|l|l|l|l|}
            \hline
            & A & B & C & D \\
            \hline
            1 & \textbf<2->{user} & \textbf<2->{deposit} & \textbf<3->{collected} & \textbf<4->{operations} \\
            \hline
            2 & & & =IF(A2 = ``Alice'', 0, B2) &  =IF(A2 = ``Alice'', SEND(``Alice'', B2), EMPTY()) \\
            \hline
            3 & & & =IF(A3 = ``Alice'', 0, B3 + C2) & =IF(A3 = ``Alice'', SEND(``Alice'', B3+C3), EMPTY()) \\
            \hline
          \end{tabular}
        }
        \caption{Bill's Spreadsheet}
      \end{figure}
    \end{center}
\end{frame}
\begin{frame}[fragile]{This What Bill's Contract Looks Like}
  \begin{center}
    \begin{figure}
      \begin{minted}[fontsize=\scriptsize]{c}
    { parameter unit ;
      storage address ;
      code { CDR ;
             SENDER ;
             CONTRACT unit ;
             IF_NONE
                 { NIL operation ; PAIR }
                 { SWAP ;
                   DUP ;
                   DUG 2 ;
                   SENDER ;
                   COMPARE ;
                   EQ ;
                   IF { SWAP ;
                        NIL operation ;
                        DIG 2 ;
                        BALANCE ;
                        PUSH unit Unit ;
                        TRANSFER_TOKENS ;
                        CONS ;
                        PAIR }
                      { DROP ; NIL operation ; PAIR } } } }
      \end{minted}
      \caption{Bill's Contract in Michelson}
    \end{figure}
  \end{center}
\end{frame}
\begin{frame}[fragile]{The High-Level Version}
  \begin{center}
    \begin{figure}
      \begin{minted}[fontsize=\scriptsize]{ocaml}
    type storage = address

    type parameter = unit

    let result (op: operation list)
                 : operation list * storage = (op, alice)

    let main ((), alice : parameter * storage) :
                             operation list * storage =
    let some_contract : unit contract option  =
    Tezos.get_contract_opt Tezos.sender in
    match some_contract with
     | Some(sender_contract) ->
       if sender = alice then
         result [Tezos.transaction
                  () Tezos.balance  sender_contract]
       else
         result ([]:operation list)
     | None ->
         result ([]:operation list)
       \end{minted}
       \caption{Bill's Contract in Ligo}
     \end{figure}
   \end{center}
\end{frame}
\begin{frame}{Spreadsheets Are Easy To Use}
  \begin{itemize}
  \item Spreadsheets is the most used computation platform.
  \item With the correct formulas, users can program autonomously.
  \item But, accessiblity may be an illusion.
  \end{itemize}
\end{frame}
\begin{frame}[fragile]{Spreadsheets Programming is Error Prone}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Until very recently, there where no tool for static analysis.
      \item In 2012, JP Morgan lost about 6 billion dollars because of
        a spreadsheet mistake.
      \item In 2010, a US federal budget proposal was based on a
        flawed economic study due to a spreadsheet error.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{tikzpicture}
          \node(a)
          {\includegraphics[width=0.9\textwidth]{jp-morgan}};
          \node
          [visible on=<2->,rotate=348,anchor=center, xshift=0mm, yshift=0mm]
          {
            \includegraphics[width=0.7\textwidth]{inflation}
          };
        \end{tikzpicture}
    \end{column}
  \end{columns}
\end{frame}
\begin{frame}[fragile]{Finding a Good Tool}
  We want a balance between accessibility, reliability and expressivity.
  \begin{center}
    \begin{tikzpicture}
      \node[buffer] {Lisa};
    \end{tikzpicture}
  \end{center}
  \begin{block}{Question}
    How to provide end users a smart contract design interface?
  \end{block}
\end{frame}
\begin{frame}{This Presentation}
  \tableofcontents
\end{frame}
\section{Structured Programming on Spreadsheet or Pull-Down Programming}
\subsection{How Users Work on Spreadsheets}
\begin{frame}{Spreadsheets 101}
  \begin{block}{Core constructs}
    \begin{itemize}
    \item Spreadsheets define a matrix of cells.
    \item Cells can contain data or an expression.
    \item Expressions may depend on other cells using a coordinate system.
    \end{itemize}
  \end{block}
\end{frame}
\begin{frame}[fragile]{Spreadsheets 101}
  This what users could do to define natural numbers.
  \begin{center}
    \animategraphics[autoplay, controls=play,scale=0.25]{6}{excel-nat-}{1}{73}
  \end{center}
\end{frame}
\begin{frame}[fragile]{Spreadsheet Facts}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      % \item<2-> Very popular among the financial community.
      % \item<3-> Computations make no side-effects, which eases formal
      %   verification.
      \item In majority spreadsheets are rectangular and grow vertically.
      \item Instead of defining each cell, they pull them down.
      \item Such spreadsheets can be seen as mutually recursive streams.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
          \includegraphics[width=0.6\textwidth]{rectangular}
          \caption{ Daniel W. Barrowy et al., OOPSLA SPLASH'18}
        \end{figure}

    \end{column}
  \end{columns}
    \begin{block}{Question}
      Is it possible to formalize structured spreadsheets are \textbf{reactive programs}?
    \end{block}
\end{frame}
\subsection{A Reactive Interpretation of Spreadsheets}
\begin{frame}[fragile]{A Correspondence with Streams}
  In Haskell
    and Lustre you can have similar definitions for natural numbers.
  \begin{itemize}
  \item In Haskell
    \begin{minted}{haskell}
      nat = 0 : map (+1) nat
    \end{minted}
  \item In Lustre
    \begin{minted}{haskell}
      nat = 0 fby (nat + 1);
    \end{minted}
  \end{itemize}
\end{frame}
\begin{frame}[fragile]{Why a New Tool?}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item However a correspondence is not always clear.
      \item Consider the following definition of Fibonacci.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \includegraphics[width=0.7\textwidth]{fib}
          % \begin{tabular}{|c|c|}
          %   \hline
          %   & A \\
          %   \hline
          %   1 & 1 \\
          % \hline
          %   2 & 1 \\
          %   \hline
          %   3 & =A1+A2\\
          %   \hline
          % \end{tabular}
          \caption{Spreadsheet Definition of Fibonacci}
        \end{figure}
    \end{column}
  \end{columns}
\end{frame}
\begin{frame}[fragile]{Corresponding Streams}
  \begin{figure}
    \includegraphics[scale=0.35]{fib-bis}
          % \begin{tabular}{|c|c|}
          %   \hline
          %   & A \\
          %   \hline
          %   1 & 1 \\
          % \hline
          %   2 & 1 \\
          %   \hline
          %   3 & =A1+A2\\
          %   \hline
          % \end{tabular}
    \caption{Spreadsheet Definition of Fibonacci}
  \end{figure}
  The correspondence begins to be more difficult explain to Bill.
  \begin{itemize}
  \item In Haskell
    \begin{minted}{haskell}
      fib = 1 : 1 : zipWith (+) fib (tail fib)
    \end{minted}
  \item In Lustre
    \begin{minted}{haskell}
      fib = 1 fby f;
      f   = 1 -> fib + (pre fib);
    \end{minted}
  \end{itemize}
\end{frame}
\begin{frame}[fragile]{What About This One?}
  \begin{figure}
    \includegraphics[scale=0.5]{a}
      % \begin{tabular}{|c|c|}
      %   \hline
      %   & A \\
      %   \hline
      %   1 & 1 \\
      %   \hline
      %   2 & 1 \\
      %   \hline
      %   3 & 1 \\
      %   \hline
      %   4 & =A1+A2+A3\\
      %   \hline
      % \end{tabular}
      \caption{A More Complicated Spreadsheet}
    \end{figure}
\end{frame}
\begin{frame}[fragile]{Corresponding Streams}
  \begin{figure}
    \includegraphics[scale=0.25]{a-bis}
    \caption{A More Complicated Spreadsheet}
  \end{figure}
  \begin{itemize}
  \item In Haskell we have to define a mapping function and use the
    \textit{right} tail.
    \begin{minted}[fontsize=\small]{haskell}
    map3 _ _ _ _ = []
    map3 f (x:xs) (y:ys) (z:zs) = f x y z : map3 xs ys zs
    plus3 x y z = x + y + z
    a = 1 : 1 : 1 : map3 plus3 a (tail a) (tail (tail a))
    \end{minted}
  \item In Lustre we need more equations to add the \textit{right}
    number of delay
    \begin{minted}[fontsize=\small]{haskell}
      a = 1 fby b;
      b = 1 fby c;
      c = 1 fby (1 -> b + (pre b) + (pre a));
    \end{minted}
  \end{itemize}
  \begin{block}{Question}
    Is the accessiblity of spreadsheets due to explicit coordinates?
  \end{block}
\end{frame}
\begin{frame}{About Existing Languages}
  \begin{itemize}
  \item Haskell is expressive but lacks of a notion of time.
  \item Lustre is safe but lacks expressivity and time is implicit.
  \end{itemize}
    \begin{block}{Questions}
      \begin{enumerate}
      \item How to interpret references to cells?
      \item How to define the \textit{right} amounts of delay or tails?
      \item How can we formalize the semantics of structured sheets?
      \end{enumerate}
    \end{block}
\end{frame}
\section{Lisa a Smart Contract DSL with Explicit Time}
\subsection{Lisa an Explicit Time Language}
\begin{frame}[fragile]{Lisa 101}
  \begin{itemize}
  \item At its core Lisa is a typed functional stream-processing language.
  \item In OCaml this program diverges:
    \begin{minted}{ocaml}
      let rec from x = x::from (x + 1)
      let nat = from 0
    \end{minted}
  \item In Lisa it doesn't:
    \begin{minted}{ocaml}
      let rec from =
        fun x ->
          (x :: thunk ((force from) (x+1)))
      let nat = from 0
    \end{minted}
  \end{itemize}
\end{frame}
% \begin{frame}[fragile]{Streams in Lisa}
%   \begin{mathpar}
%     \infer
%     [T-Force]
%     {\prove
%       {\Gamma}
%       {\hastype{\lterm}{\tyu{(\tau)}}}}
%     {\prove
%       {\Gamma}
%       {\hastype{\lforce{\lterm}}{\tau}}}

%     \infer
%     [T-Thunk]
%     {\prove
%       {\Gamma}
%       {\hastype{\lterm}{\tau}}}
%     {\prove
%       {\Gamma}
%       {\hastype{\lthunk{\lterm}}{\tyu{(\tau)}}}}\\
%     \infer
%     [T-Stream]
%     {\prove
%       {\Gamma}
%       {\hastype{\lterm}{\tau}}
%       \\
%       \prove
%       {\Gamma}
%       {\hastype{\ltermbis}{\tyu{(\tystream{\tau})}}}}
%     {\prove
%       {\Gamma}
%       {\hastype{\vstream{\lterm}{\ltermbis}}{\tystream{\tau}}}}

%     \infer
%     [T-Rec]
%     {\prove
%       {\Gamma, \hastype{\lvar}{\tyu{(\tau)}}}
%       {\hastype{\lterm}{\tau}}}
%     {\prove
%       {\Gamma}
%       {\hastype{\mu\lvar.{\lterm}}{\tau}}}
%   \end{mathpar}
%   \begin{itemize}
%   \item The tail of streams is suspended into a thunk.\\
%   \item Recursive terms are suspended in thunks.\\
%   \end{itemize}
% \end{frame}
\subsection{How Structured Spreadsheets correspond with Lisa}
\begin{frame}[fragile]{Correspondence Using the At Operator}
  \begin{itemize}
  \item The coordinates are implemented with the At operator.
  \item It is used to reference values of streams.
  \item Natural numbers:
    \begin{minted}{ocaml}
      (** Syntactic sugar
          x :* y = x :: thunk y **)

      rec nat = 0 :*
        rec nat_exp =
         ((force nat)@(fun line -> line-1) + 1)::nat_exp
    \end{minted}
  \item Fibonacci:
    \begin{minted}{ocaml}
      rec fib = 1 :* 1 :*
        rec fib_exp =
          (force fib)@(fun line -> line - 1)
          + (force fib)@(fun line -> line - 2)::fib_exp)
    \end{minted}
  \end{itemize}
\end{frame}
\begin{frame}[fragile]{And the Trickier One}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{figure}
        \includegraphics[width=0.75\textwidth]{a}
      %   \begin{tabular}{|c|c|}
      %     \hline
      %     & A \\
      %     \hline
      %     1 & 1 \\
      %     \hline
      %     2 & 1 \\
      %     \hline
      %     3 & 1 \\
      %     \hline
      %     4 & =A1+A2+A3\\
      % \hline
      %   \end{tabular}
        \caption{A Complicated Spreadsheet}
      \end{figure}
    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{figure}
      \begin{minted}[fontsize=\small]{ocaml}
    rec a = 1 :* 1 :* 1 :*
       rec a_exp =
         (force a)@(fun line -> line - 1)
         + (force a)@(fun line -> line - 2)
         + (force a)@(fun line -> line - 3)
         :: a_exp
       \end{minted}
       \caption{Implementation in Lisa}
     \end{figure}
    \end{column}
  \end{columns}
\end{frame}
\begin{frame}{From Spreadsheets to Lisa}
  \begin{itemize}
  \item Referencing a cell \textit{n}, is getting the
    \textit{n}-th value of a column.
  \item To fetch the \textit{n}-th value of a stream, we use the At operator.
  \item It \textbf{forces} the stream to be at least of length \textit{n}.
  \item The progression of time is implicit and accessible when needed.
  \item At, uses the contextual time to compute the desired observation.
  \end{itemize}
\end{frame}
\begin{frame}[fragile]{Bill's Contract in Lisa}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{minted}[fontsize=\scriptsize]{ocaml}
        (* Prelude *)

        let id = fun x -> x

        let pred = fun x -> x-1

        (* Time observation operations *)
        let previous = fun s -> (force s)@pred

        let current = fun s -> (force s)@id

        (** Syntactic sugar
            x :* y = x :: thunk y **)

        (* Blockchain operations *)
        let send =
          fun who ->
            fun what -> [(who, what)]

        let empty = []
      \end{minted}
    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{minted}[fontsize=\scriptsize]{ocaml}
        (* Mutually recursive streams
           for users, deposits, collected coins
           and operations to commit *)

        rec user = input "user" :: user

        and deposit = input "deposit" :: deposit

        and collected =
          (if (current user) = "Alice" then 0
           else (current deposit))
          :* (rec next_collected =
                (if (current user) = "Alice" then 0
                 else (current deposit)
                      + (previous collected))
                :: next_collected)

        and operations =
          (if (current user) = "Alice" then
            send "Alice" (current deposit)
           else empty)
          :* (rec next_operations =
                (if (current user) = "Alice" then
                   send "Alice"
                     ((current deposit)
                     + (previous collected))
                 else empty):: next_operations
      \end{minted}
    \end{column}
  \end{columns}
\end{frame}
\subsection{Formalization of Lisa}
\begin{frame}[fragile]{Lisa's Syntax}
      \begin{figure}
        \begin{center}
          \[
            \begin{array}{rclr}
              \lterm & ::=  & \lvar                  & \textrm{Variable}        \\
                     & \mid & \lfun{\lvar}{\lterm}  & \textrm{Abstraction}     \\
                     & \mid & \lapp{\lterm}{\lterm}        & \textrm{Application}     \\
                     & \mid & \lforce{\lterm}          & \textrm{Force}           \\
                     & \mid & \lthunk{\lterm}          & \textrm{Thunk}           \\
                     & \mid & \lfby{\lterm}{\lterm}    & \textrm{Stream Constructor}     \\
                     & \mid & \lnobs{\lterm}{\lterm}    & \textrm{Observation}     \\
                     & \mid & \ltuple{\lmany\lterm}    & \textrm{Tuples}           \\
                     & \mid & \lmutuple{\lmany\lvar}{\lmany\lterm} & \textrm{Recursive Tuple} \\
                     & \mid & \lprimitive{\lmany{\lterm}} & \textrm{Primitive Application} \\
                     & \mid & \lconstant & \textrm{Constants} \\
            \end{array}
          \]
          \caption{Lisa Syntax}
        \end{center}
      \end{figure}
\end{frame}
\begin{frame}{Call-By-Push-Value: Computations Do and Values Are!}
  \begin{itemize}
  \item Call-By-Push-Value (CBPV) distinguishes values and computations \textbf{syntactically}.
  \item Computations can be \textbf{suspended} as values using \textit{thunks}.
  \item Thunked computations can be \textbf{resumed} by \textit{forcing} them.
  \item Functions are \textbf{computations}, not values.
  \end{itemize}
\end{frame}
\begin{frame}{From Lisa to CBPV}
  \begin{itemize}
  \item Lisa is translated to CBPV with Streams and the At operator.
  \item CBPV with Streams and At is formalized using a monadic translation.
  \item The monad abstracts the progress of time.
  \end{itemize}
  \begin{center}
    \begin{figure}
      \begin{tikzpicture}
        \node (lisas) [language] {Structured Spreadsheet};
        \node (lisa)  [language, right of=lisas, xshift = 2.5cm] {Lisa};
        \node (cbpvs) [language, right of=lisa, xshift = 2.5cm] {CBPV + Streams + At};
        \node (cbpv)  [language, below of=cbpvs, yshift = -3cm] {CBPV + Streams};
        \draw [arrow] (lisas) -- (lisa);
        \draw [arrow] (lisa) -- (cbpvs);
        \draw [arrow] (cbpvs) -- node [anchor=east] {Monadic Translation}(cbpv);
      \end{tikzpicture}
      \caption{Compilation Scheme}
    \end{figure}
  \end{center}
\end{frame}
\section{Ongoing and Future Work}
\subsection{What is Done}
\begin{frame}{Front-End of an Interpreter for Structured Spreadsheets}
   \begin{itemize}
   \item Formalized a spreadsheet system to design smart contracts.
   \item An implementation of a Lisa interpreter.
   \item A proof of concept compiler to translate a structured spreadsheet to CBPV.
   \item Example contracts written as structured spreadsheets.
   \item Code, examples and work in progress: \url{https://gitlab.com/cg-thesis/lisa}
   \item Started the formalization of Lisa and its compilation to CPBV.
   \item Started the correctness proof of the compilation.
   \end{itemize}
\end{frame}
\subsection{Future Work}
\begin{frame}{Generation of Michelson Code}
  \begin{itemize}
  \item Clock-directed code generation, Marc Pouzet et al.
  \item Incrementalization of the program to get a reactive function
    by the derivative of the program.
  \end{itemize}
  \begin{center}
    \begin{figure}
      \scalebox{0.75}{
      \begin{tikzpicture}
        \node (lisas) [language] {Structured Spreadsheet};
        \node (lisa)  [language, right of=lisas, xshift = 2.5cm] {Lisa};
        \node (cbpvs) [language, right of=lisa, xshift = 2.5cm] {CBPV + Streams + At};
        \node (cbpv)  [language, below of=cbpvs, yshift = -1cm] {CBPV + Streams};
        \node (mich)  [language, below of=cbpv, yshift = -1cm] {Michelson};
        \draw [arrow] (lisas) -- (lisa);
        \draw [arrow] (lisa) -- (cbpvs);
        \draw [arrow] (cbpvs) -- node [anchor=east] {Monadic Translation}(cbpv);
        \draw [arrow] (cbpv) -- node [anchor=east] {?}(mich);
      \end{tikzpicture}}
      \caption{Compilation Scheme}
    \end{figure}
  \end{center}
\end{frame}
\begin{frame}{Clock-Directed Code Generation}
  \begin{itemize}
  \item The compilation of higher-order reactive languages is still an
    open question.
  \item In his thesis, Adrien Guatto studied the usage of integer
    clocks on compilation of functional reactive programs to digital
    circuits (e.g. VHDL).
  \item We want to investigate the possibility to generate an
    iterative step function.
  \end{itemize}
\end{frame}
\begin{frame}[fragile]{Compilation by Static Differentiation}
  \begin{itemize}
  \item In ESOP’19, Yann Régis-Gianas and his coauthors have shown a
    novel technique to compute a derivative of a functional program,
    that is for all function $f : A \rightarrow B$:
    \[
      \begin{array}{c}
        D\llbracket f \rrbracket : A \rightarrow \Delta A \rightarrow \Delta B\\
        f\,(x \oplus dx) = f\,x \oplus D\llbracket f \rrbracket \,x\,dx
      \end{array}
    \]
  \item A spreadsheet defines a function $f$ from list of calls to list of
    operations.
  \item The underlying smart contract may be an incrementalization of
    $f$, i.e. $D\llbracket f \rrbracket$
  \end{itemize}
\end{frame}
\begin{frame}{Open Questions}
  \begin{itemize}
  \item What is the class of contracts that can be naturally encoded
    as structured spreadsheets?
  \item The At operator is very expressive but, can we statically verify
    its proper usage, i.e. only observe past or current, non-cyclic, values ?
  \end{itemize}
\end{frame}
\begin{frame}{Roadmap}
  \begin{itemize}
  \item Finish the front-end with a usable proof-of-concept and write
    an article by December 2021.
  \item Results on the back-end by August 2022.
  \item Start writing the manuscript in September 2022.
  \end{itemize}
\end{frame}
\section{Questions}
  \begin{frame}
    \vfill
    \centering
    \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
      \usebeamerfont{title}Questions ? \par%
    \end{beamercolorbox}
    \vfill
  \end{frame}
\end{document}
