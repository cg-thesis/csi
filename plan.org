* Intro and context
** Programmable transactions
   - How to make a secure transaction without a trusted third party?
   - You use a blockchain, a distributed non forgeable ledger.
   - Transactions are recorded and performed on computers using a
     specific protocol.
   - Bonus: they are programmable.

** You said programmable?
   - Yes!
   - SmartContracts run embedded in the blockchain and may handle critical
     amounts of tokens.
   - These programs can't be modified and they are publicly hosted and
     executed on the blockchain. Hence the contract trait.

** A funding-pot contract for Alice
  - Alice worked hard this summer to buy herself a computer for the first year
    of her computer sciences degree.
  - Bob, her cat, spilled her cup of coffee while she was studying.
  - Her friends set up a funding-pot using a smartcontract on a french
    cryptocurrency so she can buy a new latop.
  - This use case may be quite common, there are several popular
    funding pots apps.
  - Writing a smartcontract can be difficult, they are written in low
    level or domain specific languages.
*** An informal spec:
    This contract collects the desposits and only Alice can
    withdraw the desposited money.
***  Question
    How to provide programmers a simple smart contract design interface?

		Accessibility
	  (Traditional spreadsheets)
		      Λ
		     / \
		    /   \
		   /     \
		  /       \
		 /         \
		/   Goal    \
	       /             \
	      /_______________\
   Reliability                 Expressivity
(Safe DSL, lustre)       (High level languages Haskell, Solidity)

** Use a spreadsheet!
   - You use a spreadsheet!
   - Really, you want to use Excel?
   - Sure, spreadsheets offer interesting properties.

** The bright side
   - It is the most used computation platfrom.
   - Very popular in the finantial comunity.
   - Computations make no side effect, which may be suitable for
     fromal verification.

** The dark side
   - Many usages and different techniques.
   - Some are unsafe.
   - In 2012, JP Morgan lost 6billion dollars
   due to a wrong usage [source] (a reformuler).

** Towards structured programming
*** Goal
    - Not making the usage difficult for endusers.
    - Focus on core features like cell referencing
      and copying a definition by pulling it down a column.
    - Compatible with reliable a execution model.
*** Idea
    - A line represents an execution.
    - Columns are snychronous mutually recursive streams.
    - Therefore a contract is a reactive program.
    - It is only possible to reference a previous or current line.

** This presentation
   - Introduce structured and accessible programming on
     spreadsheets.
   - Porpose a synchronous yet very expressive language to interpret
     sheets.
   - Discusses the design of a compiler from structured spreadsheet to
     Michelson.

* Structured programming on spreadsheets or pull-down programming
** Core constructs of spreadsheets
   - Referencing other cells is a key feature of spreadsheets.
   - References to cell instances are explicit and absolute.
   - Reuse expressions by pulling it down and update cell references
     automatically, natural numbers can be written as:
   |     | A           |
   |   1 | 0           |
   |   2 | =A1 + 1     |
   | ... | *pull-down* |
** Streams offer a similar and safer approach
   - In Haskell and Lustre a similar definition uses an implicit
     reference to the `previous` value of `nat` where there is an
     implicit notion of time

   ```haskell
   nat = 0 : map succ nat
   ```

   and

   ```lustre
   nat = 0 fby succ nat;
   ```
** Translating sheets, frist try.
   - However the transalation is not always clear
   - Consider the following definition of Fibonacci
   |   |            A |
   | 1 |            1 |
   | 2 |            1 |
   | 3 | =A1 + A2     |

   in Haskell we can write
   ```haskell
   fib = 1 : 1 : zipWith (+) fib (tail fib)
   ```
   and
   ```lustre
   fib = 1 fby f;
   f    = 1 -> fib + pre fib;
   ```
   But what about the following sheet?
   |   |             A |
   | 1 |             1 |
   | 2 |             1 |
   | 3 |             1 |
   | 4 | =A1 + A2 + A3 |

   in Haskell we have to define a mapping function and get the *right*
   tail in Lustre we need to define intermediate equations to add
   *right* amounts of delay.

  ```haskell
   map3 _ _ _ _ = []
   map3 f (x:xs) (y:ys) (z:zs) = f x y z : map3 xs ys zs
   a = 1 : 1 : 1 : map3 (\x y z -> x + y + z) a (tail a) (tail (tail a))
   ```

   and

   ```
   a = 1 fby b;
   b = 1 fby c;
   c = 1 fby 1 -> b + pre b + pre a;
   ```
** Existing synchronous languages
   - Haskell is expressive but lacks of a notion of time.
   - Lustre is safe but lacks expressivity and time is implicit.
   - They are not well-suited targets to translate sheets.
*** Questions
    - How to translate cell references?
    - How to define the *right* amounts of delay or tails?
    - How can we formalize the semantics of such structured sheets?
** In practice, the funding pot contract
   - two inputs, user and deposit
   - a state to compute the collected amount
   - operations to perform
   -
#+BEGIN_EXAMPLE
| user | deposit | collected                                            | operations                                                |
|      |         | =IF(user[1] = "Alice", 0, deposit[1])                | =IF(user[1] = 0, MAP(PAIR(0, deposit[1])), MAP.EMPTY())   |
|      |         | =IF(user[2] = "Alice", 0, deposit[1] + collected[2]) | =IF(user[2] = "Alice", MAP(PAIR(0,value[1])), MAP.EMPTY())|
#+END_EXAMPLE
* Lisa a functionnal synchronous language with explicit time
** To interpret structured speadsheets we introduce Lisa
   - a typed stream processing language
   - features functionnal values
   - lazy general recursion
   - explicit notion of time
** Not just another synchronous language
   - Lisa has an explicit notion of time
   - An operator, At to translate cell references.
   - Lisa has also general programming trait as expressive as other
     high level languages
   - This trait integerates naturally with the synchronous paradigm in
     contrast with traditional spreadhseets systems that provide
     matrix like interface to sheets.

** Informal semantics of the at operator
     - How to define the *right* amounts of delay or tails?
     - The At operator abstracts away this problem.
     - Considering that a line is an execution then,
       referencing a cell of previous a line is like consuming a value
       at a previous time.
     - At fetches a value at an arbitrary relative time of given stream.
     - The time is relative to the current execution time.
     - At is similar to the `nth` function on lists but also handles
       time properly.

** General recursion
   - Like OCaml, Lisa evaluation strategy is call-by-value.
   - In call by value, writing safe recursive terms is bit tricky.
   - OCaml accepts some terms:
     ```
     let x = 0::y and y = 1::x
     ```
     But rejects some others
     ```ocaml
     let rec nat = 0 ::List.map succ nat
     ```
     And accepts this one that diverges.
     ```ocaml
     let rec from x = x::from x in nat = from 0
     ```

** Lazyness to the rescue
   - Lisa uses thunks to provide lazy evaluation.
   - General recursion is also lazy.
   - Instead of substituting the term with itself we reduce it with
     its lazy counterpart.

     Γ,(x, thunk t) ⊢ t ⇓ⁿ v
     ——————————————————————— E-Fix
	 Γ ⊢ μ x. t ⇓ⁿ v

   - An alternative definition of `nat` in Lisa that does not diverge.
     ```lisa
     let rec from = fun n -> x :: thunk ((force from) (n+1))
     rec nat = from 0
     ```

** Translating sheets, sencond try

   - Translating previous examples to Lisa is straightforward.
   - Cell references are translated using the At operator.
   - At references are relative with respect to line or rather time
     being evaluated.

   natural numbers
   ```
   rec nat = 0 :: (rec nat_exp = succ nat@(fun line -> line - 1) :: nat_exp)
   ```
   fibonacci
   ```
   rec fibo = 1 :: 1 ::
     (rec fibo_exp = fibo@(fun line -> line - 1) + fibo@(fun line -> line - 2)::fibo_exp)
   ```
   or even the more complicated example
   ```
   rec a = 1 :: 1 :: 1 ::
     (rec a_exp =
	a@(fun line -> line - 1) + a@(fun line -> line - 2) + a@(fun line -> line - 3) :: a_exp)
    ```

** The funding pot example in Lisa
   ```lisa
   let id = fun x -> x

   let pred = fun x -> x-1

   (* Time observations operations *)
   let previous = fun s -> s@pred

   let current = fun s -> s@id

   let send = fun to -> fun what -> [(to, what)]

   let empty = []

   (* Mutually recursive streams for users, deposits, collected coins
      and operations to commit *)

   rec user = input "user" :: user

   and deposit = input "deposit" :: deposit

   and collected =
     (if (current user) = "Alice" then 0 else (current deposit))
     :: rec next_collected =
	  (if (current user) = "Alice" then 0
	   else (current deposit) + (previous collected)) :: next_collected

   and operations =
     (if (current user) = "Alice" then send "Alice" (current deposit) else empty)
     :: rec next_operations =
	  (if (current user) = "Alice" then send "Alice" (previous collected) else empty)
	  :: next_operations
   ```

** On time-safety
   - At the moment, we do not know how to check the time safety of
     this programs.
   - However, in sheets, cell references range over 0 to the current
     time which is safe.
   - Investigate the usage of an access verification system for
     recursive terms (papier de Gabriel S.)
** Formalization of Lisa
   - Call-by-push-value is a language capable to subsume the semantics
     of both call-by-value and call-by-name for the λ-calculus
   - An important caracteristic of CBPV is the syntactic separation
     between computations, terms that can reduce,
     and values, terms that can not reduce.
   - The recursion of Lisa is very similar to the one in CBPV.
   - In order to translate Lisa to CBPV, we have extended CBPV with
     streams and the At operator.
   - We have defined a monad in CBPV and a monadic translation to
     fromalize the progress time and time operations.

* Existing and future work
** So far we have:
   - An implementation of a lisa interpreter, small and big-step
   - A compiler from CSV spreadsheets using a forumla language to
     monadic CBPV
   - The compiler transaltes lisa, then cbpv+streams then monadic
     cbpv.
** Design of a compiler from spreadsheet to michelson
   - translation to micheslon a stack low level smart contract for the
     Tezos blockchain.
   - spreadsheet defines a function f from a list of contract
     computations to a list of blockchain transactions
   - underlying smart contract may simply be an incrementalization
