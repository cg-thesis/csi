.PHONY: all main show watch clean

PDFLATEX?=pdflatex --shell-escape
LATEXMK?=latexmk -pdf -pdflatex="$(PDFLATEX)"

TARGET=main.pdf

all: main

main:
	$(LATEXMK) $(TARGET:.pdf=.tex)

show:
	$(LATEXMK) -pv  $(TARGET:.pdf=.tex)

watch: show
	$(LATEXMK) -pvc -interaction=nonstopmode $(TARGET:.pdf=.tex)

clean:
	$(LATEXMK) -c $(TARGET:.pdf=.tex)

mrproper:
	$(LATEXMK) -C $(TARGET:.pdf=.tex)
